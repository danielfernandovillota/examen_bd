create database examenes;
use examenes;

create table temas(
	id int auto_increment primary key,
    nombre varchar(45) not null
);

create table tipos_preguntas(
	id int auto_increment primary key,
    tipo varchar(45) not null
);

create table personas(
	id int auto_increment primary key,
    nombre varchar(45) not null,
    apellido varchar(45) not null,
    tipo_documento char(2) not null,
    numero_documento varchar(15) not null unique,
    correo varchar(45) not null,
    index(numero_documento)
);

create table modulos(
	id int auto_increment primary key,
    nombre varchar(45) not null
);

create table partes_examenes(
	id int auto_increment primary key,
    nombre varchar(45) not null
);

create table preguntas(
	id int auto_increment primary key,
    descripcion varchar(45) not null,
    respuesta varchar(45) not null
);

create table resultados(
	id int auto_increment primary key,
    fecha varchar(45) not null,
    valor boolean not null,
    porcentaje int not null,
    index(fecha)
);

alter table personas add column tema_id int not null, add
constraint fk_personas_temas foreign key (tema_id) references
temas(id);

alter table modulos add column tema_id int not null, add
constraint fk_modulos_temas foreign key (tema_id) references
temas(id);

alter table partes_examenes add column modulo_id int not null, add
constraint fk_partes_examenes_modulos foreign key (modulo_id) references
modulos(id);

alter table preguntas add column parte_examen_id int not null, add
constraint fk_preguntas_partes_examenes foreign key (parte_examen_id) references
partes_examenes(id);

alter table preguntas add column tipo_pregunta_id int not null, add
constraint fk_preguntas_tipos_preguntas foreign key (tipo_pregunta_id) references
tipos_preguntas(id);

alter table resultados add column pregunta_id int not null, add
constraint fk_resultados_preguntas foreign key (pregunta_id) references
preguntas(id);

alter table resultados add column persona_id int not null, add
constraint fk_resultados_personas foreign key (persona_id) references
personas(id);


insert into temas(nombre)values('Matematicas');
insert into temas(nombre)values('Ingles');

insert into personas(nombre,apellido,tipo_documento,numero_documento,correo,tema_id)
values('daniel','torres','CC','1085345270','daniel@gmail.com',1);
insert into personas(nombre,apellido,tipo_documento,numero_documento,correo,tema_id)
values('maria','mora','CC','1085345271','maria@gmail.com',1);
insert into personas(nombre,apellido,tipo_documento,numero_documento,correo,tema_id)
values('ana','lopez','CC','1085345272','ana@gmail.com',2);

insert into modulos(nombre,tema_id)values('calculo diferencial',1);
insert into modulos(nombre,tema_id)values('integrales',1);
insert into modulos(nombre,tema_id)values('ingles I',2);

insert into partes_examenes(nombre,modulo_id)values('parte 1',1);
insert into partes_examenes(nombre,modulo_id)values('parte 2',2);
insert into partes_examenes(nombre,modulo_id)values('parte 1',3);

insert into tipos_preguntas(tipo)values('texto plano');
insert into tipos_preguntas(tipo)values('seleccion unica');
insert into tipos_preguntas(tipo)values('seleccion multiple');

insert into preguntas(descripcion,respuesta,parte_examen_id,tipo_pregunta_id)
values('caul es la integral de x ?','x^2/2 + C',2,2);
insert into preguntas(descripcion,respuesta,parte_examen_id,tipo_pregunta_id)
values('Que es una funcion ?','Es una magnitud',1,1);
insert into preguntas(descripcion,respuesta,parte_examen_id,tipo_pregunta_id)
values('Ingeniero en ingles ?','Engyner',3,1);

insert into resultados(fecha,valor,porcentaje,pregunta_id,persona_id)
values('05/09/2023',True,10,1,1);
insert into resultados(fecha,valor,porcentaje,pregunta_id,persona_id)
values('05/09/2023',True,10,2,2);
insert into resultados(fecha,valor,porcentaje,pregunta_id,persona_id)
values('05/09/2023',False,10,3,3);

select*from temas;
select*from personas;
select*from modulos;
select*from partes_examenes;
select*from tipos_preguntas;
select*from preguntas;
select*from resultados;

-- CONSULTAR TODOS LOS TEMAS EXISTENTES PARA EXAMENES
select*from temas;

-- CONSULTAR EL TOTAL DE PREGUNTAS EXISTENTES EN LA BD
select count(id) as total_preguntas_existentes from preguntas;

-- CONSULTAR LOS RESULTADOS ENTRE EL MES DE ENERO Y JUNIO
select*from resultados where fecha BETWEEN '01/01/2023' AND '06/31/2023';

-- CONSULTAR EL RESULTADO DEL ESTUDIANTE CON CEDULA 1085345270
select * from resultados r
inner join personas p
on (r.persona_id=p.id) where p.numero_documento='1085345270';

-- CONSULTAR LOS ESTUDIANTES QUE RESPONDIERON MAL CON SU PREGUNTA Y RESPUESTA
select * from resultados r
inner join personas p
on (r.persona_id=p.id)
inner join preguntas pr
on (r.pregunta_id=pr.id) where r.valor=FALSE;

-- CONSULTAR LOS ESTUDIANTES QUE RESPONDIERON BIEN CON SU PREGUNTA Y RESPUESTA
select * from resultados r
inner join personas p
on (r.persona_id=p.id)
inner join preguntas pr
on (r.pregunta_id=pr.id) where r.valor=TRUE;

-- CUANTOS MODULOS TIENE CADA TEMA
select count(m.id) as cantidad_partes, t.nombre from  modulos m
inner join temas t
on (m.tema_id=t.id) group by t.nombre;

-- CUANTOS PREGUNTAS TIENE CADA TEMA
select count(f.id) as cantidad_preguntas, t.nombre from  modulos m
inner join temas t
on (m.tema_id=t.id)
inner join partes_examenes p
on (p.modulo_id=m.id)
inner join preguntas f
on (f.parte_examen_id=p.id)
group by t.nombre;
